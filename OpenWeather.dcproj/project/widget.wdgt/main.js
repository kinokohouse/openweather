// OpenWeather 1.0
// (c) 2019 Kinoko House. MIT License.
// Icons by Dorova Interactive, CC-BY 4.0 Int'l (clickable links on back of widget).
//
// Replacement for the macOS weather widget that stopped working.
// Tested only on 10.6 and 10.13, should work on 10.4.3 and up but caveat emptor.


var sizeToggle = true;
var animationRunning = false;
var isBusy = false;
var animationTrapTimer = null;

var sites = ["https://dovora.com/","https://creativecommons.org/licenses/by/4.0/","https://openweathermap.org/"];

var currentweather;
var weathertimer = null;
var forecast;
var forecasttimer = null;
var currentweatherlist = {};
var dayArray = [];
var daytime = true;

var city = "Kralingen,NL";
var customname = "";
var apikey = ""
var preferredunits = "c";

var debug = false;

const timervalue = 1100;
const regularcheckinterval = 600000;


function load() {
    dashcode.setupParts();
    overhaulBack();
    getPrefs();
    startRunning();
}

function remove() {
    isBusy = false;
    killAllTimers();
    killPrefs();
}

function hide() {
    isBusy = false;
    killAllTimers();
    copyPrefs();
}

function show() {
    getPrefs();
    startRunning();
}

function sync() {

}

function showBack(event) {
    killAllTimers();
    isBusy = false;
    copyPrefsToBackOfWidget();
    var front = document.getElementById("front");
    var back = document.getElementById("back");
    if (window.widget) {
        widget.prepareForTransition("ToBack");
    }
    front.style.display = "none";
    back.style.display = "block";
    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
}

function showFront(event) {
    var front = document.getElementById("front");
    var back = document.getElementById("back");
    if (window.widget) {
        widget.prepareForTransition("ToFront");
    }
    front.style.display="block";
    back.style.display="none";
    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
    copyPrefs();
    startRunning();
}

if (window.widget) {
    widget.onremove = remove;
    widget.onhide = hide;
    widget.onshow = show;
    widget.onsync = sync;
}

function startRunning() {
    if (isBusy) return;
    if (debug) {
        alert("Started");
    }
    isBusy = true;
    if (weathertimer) {
        clearTimeout(weathertimer);
        weathertimer = null;
    }
    weathertimer = setTimeout(getWeather, timervalue);
}

function toggleSize() {
    if (animationRunning) return;
    animationRunning = true;
    duration = 300;
    interval = 13;
    if (sizeToggle) {
        start = 104;
        finish = 0;
    } else {
        start = 0;
        finish = 104;
    }
    handler = function(animation, current, start, finish) {
        document.getElementById("middle-div").style.height = (20 + current) + "px";
        document.getElementById("bottom").style.top = (60 + current) + "px";
        document.getElementById("info").style.bottom = (171 - current) + "px";
        document.getElementById("shadow-front").style.height = (50 + current) + "px";
    }
    new AppleAnimator(duration, interval, start, finish, handler).start();
    sizeToggle = !sizeToggle;
    animationTrapTimer = setInterval(resetAnimationFlag, 600);
}

function resetAnimationFlag() {
    animationRunning = false;
    clearTimeout(animationTrapTimer);
}

function getWeather() {
    if (weathertimer) {
        clearTimeout(weathertimer);
        weathertimer = null;
    }
    if (apikey === "") {
        keyEmpty();
        return;
    }
    var requestString = "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=" + apikey;
    if (debug) {
        alert("WEATHER: " + requestString);
    }
    var wtx = new XMLHttpRequest();
    wtx.open("GET", requestString);
    wtx.send();

    wtx.onload = function() {
        if (wtx.status != 200) {
            if (debug) {    
                alert("WEATHER: Error " + wtx.status+ " : " + wtx.statusText);
            }
            if (wtx.status == 401) keyInvalid();
            if (wtx.status == 404) cityInvalid();
        } else {
            if (debug) {
                alert("WEATHER: Done, got " + wtx.response.length + " bytes");
            }
            weather = eval("(" + wtx.responseText + ")");
            if (weather.cod == 429) {
                limitReached()
                return;
            }
            if (debug) {
                alert(weather);
            }
            parseWeather();
            if (debug) {
                alert(currentweatherlist);
            }
            forecasttimer = setTimeout(getForecast, timervalue);
        }
    }

    wtx.onerror = function() {
        if (debug) {
            alert("WEATHER: Request failed");
        }
        killAllTimers();
    }
}

function getForecast() {
    if (forecasttimer) {
        clearTimeout(forecasttimer);
        forecasttimer = null;
    }
    var requestString = "https://api.openweathermap.org/data/2.5/forecast?q=" + city + "&appid=" + apikey;
    if (debug) {
        alert("FORECAST: " + requestString);
    }
    var fcx = new XMLHttpRequest();
    fcx.open("GET", requestString);
    fcx.send();

    fcx.onload = function() {
        if (fcx.status != 200) {
            if (debug) {
                alert("FORECAST: Error " + fcx.status+ " : " + fcx.statusText);
            }
        } else {
            if (debug) {
                alert("FORECAST: Done, got " + fcx.response.length + " bytes");
            }
            forecast = eval("(" + fcx.responseText + ")");
            if (debug) {
                alert(forecast);
            }
            parseForecast();
            if (debug) {
                alert(dayArray);
            }
            populateWidget();
            weathertimer = setTimeout(getWeather, regularcheckinterval);
        }
    }

    fcx.onerror = function() {
        if (debug) {
            alert("FORECAST: Request failed");
        }
    }
}

function keyEmpty() {
    clearAllText();
    clearAllIcons("error-no-key");
    document.getElementById("location").innerText = "Enter API Key";
    document.getElementById("location").title = "You have not yet entered your OpenWeather API key, which is mandatory. If you do not have one yet, you can get one for free at openweathermap.org.";
}

function keyInvalid() {
    clearAllText();
    clearAllIcons("error-key-invalid");
    document.getElementById("location").innerText = "API Key Error";
    document.getElementById("location").title = "Your OpenWeather API key appears to be incorrect. Please flip over the widget and double-check the API key you have entered.";
}

function cityInvalid() {
    clearAllText();
    clearAllIcons("error-city-invalid");
    document.getElementById("location").innerText = "No such place";
    document.getElementById("location").title = "Apparently, " + city + " does not appear to be on any map that OpenWeather knows of. Please recheck your input, and when in doubt, follow the tooltips on the back of this widget.";
}

function limitReached() {
    clearAllText();
    clearAllIcons("error-data-limit-reached");
    document.getElementById("location").innerText = "Limit reached";
    document.getElementById("location").title = "You have exceeded the maximum number of calls for a single day, which means your account is temporarily blocked from use until you upgrade to a paid account. Actually, this should never have happened; how many instances of the widget were you running?";
}

function populateWidget() {
    if (daytime) {
        document.getElementById("top_image").src = "Images/top-day.png";
        document.getElementById("middle_image").src = "Images/middle-day.png";
        document.getElementById("bottom_image").src = "Images/bottom-day.png";
    } else {
        document.getElementById("top_image").src = "Images/top-night.png";
        document.getElementById("middle_image").src = "Images/middle-night.png";
        document.getElementById("bottom_image").src = "Images/bottom-night.png";
    }
    if (customname == "") {
        document.getElementById("location").innerText = currentweatherlist.city;
    } else {
        document.getElementById("location").innerText = customname;
    }
    var lotemp;
    var hitemp;
    var unit = "°C";
    if (preferredunits == "c") {
        lotemp = currentweatherlist["loc"];
        hitemp = currentweatherlist["hic"];
    } else if (preferredunits == "f") {
        lotemp = currentweatherlist["lof"];
        hitemp = currentweatherlist["hif"];
        unit = "°F";
    } else {
        lotemp = currentweatherlist["lok"];
        hitemp = currentweatherlist["hik"];
        unit = "K";
    }
    var titleString = currentweatherlist["desc"];
    if (lotemp != hitemp) {
        titleString += "\rTemperatures between " + lotemp + unit + " and " + hitemp + unit;
    }
    titleString += "\rPressure: " + currentweatherlist["pres"];
    titleString += "\rHumidity: " + currentweatherlist["hum"];
    titleString += "\rWind: " + currentweatherlist["windspeed"] + ", " + currentweatherlist["winddir"];
    document.getElementById("location").title = titleString;
    if (preferredunits == "c") {
        document.getElementById("temp").innerText = currentweatherlist.tempc + "°";
    } else if (preferredunits == "f") {
        document.getElementById("temp").innerText = currentweatherlist.tempf + "°";
    } else {
        document.getElementById("temp").innerText = currentweatherlist.tempk + "K";
    }
    document.getElementById("iconimg").src = "Images/" + currentweatherlist.icon + ".png";
    for (i = 0; i < 5; i++) {
        document.getElementById("day" + (i + 1)).innerText = dayArray[i].day;
        if (preferredunits == "c") {
            document.getElementById("lo" + (i + 1)).innerText = dayArray[i].loc;
            document.getElementById("hi" + (i + 1)).innerText = dayArray[i].hic;
        } else if (preferredunits == "f") {
            document.getElementById("lo" + (i + 1)).innerText = dayArray[i].lof;
            document.getElementById("hi" + (i + 1)).innerText = dayArray[i].hif;
        } else {
            document.getElementById("lo" + (i + 1)).innerText = dayArray[i].lok;
            document.getElementById("hi" + (i + 1)).innerText = dayArray[i].hik;
        }
        document.getElementById("weather" + (i + 1) + "img").src = "Images/" + dayArray[i].icon + ".png";
    }
}

function getWindDirection(deg) {
    if (deg > 337.5) return "N";
    if (deg > 292.5) return "NW";
    if (deg > 247.5) return "W";
    if (deg > 202.5) return "SW";
    if (deg > 157.5) return "S";
    if (deg > 122.5) return "SE";
    if (deg >  67.5) return "E";
    if (deg >  22.5) return "NE";
    return "N";
}

function parseWeather() {
    currentweatherlist = {};
    currentweatherlist["city"] = weather.name;
    currentweatherlist["icon"] = weather.weather[0].icon;
    if (currentweatherlist["icon"].substr(2,1) == "d") {
        currentweatherlist["pod"] = "day";
        daytime = true;
    } else {
        currentweatherlist["pod"] = "night";
        daytime = false;
    }
    var dt = weather.dt;
    var sunrise = weather.sys.sunrise;
    var sunset = weather.sys.sunset;
    if ((sunrise < dt) && (dt < sunset)) {
        currentweatherlist["pod"] = "day";
        daytime = true;
        currentweatherlist.icon = currentweatherlist.icon.substr(0,2) + "d";
    } else {
        currentweatherlist["pod"] = "night";
        daytime = false;
        currentweatherlist.icon = currentweatherlist.icon.substr(0,2) + "n";
    }
    currentweatherlist["tempc"] = Math.round(weather.main.temp - 273.15);
    currentweatherlist["tempf"] = Math.round(((weather.main.temp - 273.15) * 1.8) + 32);
    currentweatherlist["tempk"] = Math.round(weather.main.temp);
    currentweatherlist["desc"] = weather.weather[0].description.charAt(0).toUpperCase() + weather.weather[0].description.slice(1);
    currentweatherlist["pres"] = weather.main.pressure + " hPa";
    currentweatherlist["hum"] = weather.main.humidity + "%";
    currentweatherlist["hic"] = Math.round(weather.main.temp_max - 273.15);
    currentweatherlist["loc"] = Math.round(weather.main.temp_min - 273.15);
    currentweatherlist["hif"] = Math.round(((weather.main.temp_max - 273.15) * 1.8) + 32);
    currentweatherlist["lof"] = Math.round(((weather.main.temp_min - 273.15) * 1.8) + 32);
    currentweatherlist["hik"] = Math.round(weather.main.temp_max);
    currentweatherlist["lok"] = Math.round(weather.main.temp_min);
    currentweatherlist["windspeed"] = weather.wind.speed + " m/s";
    currentweatherlist["winddir"] = getWindDirection(weather.wind.deg);
}

function parseForecast() {
	var firstDay = true;
    dayArray = [];
    var dayOfWeek;
    var oldDayOfWeek = new Date(forecast.list[0].dt * 1000).toString().substr(0,3).toUpperCase();
    var mintemp = 1000;
    var maxtemp = -1000;
    var currentDayList = {};
    var weatherlist = {};
    for (i = 0; i < 40; i++) {
        var dayOfWeek = new Date(forecast.list[i].dt * 1000).toString().substr(0,3).toUpperCase();
        if ((dayOfWeek != oldDayOfWeek) || i == 39) {
            if (weatherlist.length > 1) {
                theIcon = Object.keys(weatherlist).reduce(function(a, b){ return weatherlist[a] > weatherlist[b] ? a : b });
                if (weatherlist[theIcon] == 1) {
                    theIcon = Object.keys(weatherlist)[Math.round(weatherlist.length / 2) - (weatherlist.length % 2)];
                }
            } else {
                theIcon = Object.keys(weatherlist)[0];
            }
            if (firstDay) {
            	theIcon = theIcon + currentweatherlist.icon.substr(2,1);
            } else {
            	theIcon = theIcon + "d";
            }
            currentDayList["day"] = oldDayOfWeek;
            currentDayList["icon"] = theIcon;
            currentDayList["hic"] = Math.round(maxtemp - 273.15);
            currentDayList["loc"] = Math.round(mintemp - 273.15);
            currentDayList["hif"] = Math.round(((maxtemp - 273.15) * 1.8) + 32);
            currentDayList["lof"] = Math.round(((mintemp - 273.15) * 1.8) + 32);
            currentDayList["hik"] = Math.round(maxtemp);
            currentDayList["lok"] = Math.round(mintemp);
            dayArray.push(currentDayList);
            oldDayOfWeek = dayOfWeek;
            currentDayList = {};
            weatherlist = {};
            mintemp = 1000;
            maxtemp = -1000;
            firstDay = false;
        }
        newmintemp = forecast.list[i].main.temp_min;
        if (newmintemp < mintemp) mintemp = newmintemp;
        newmaxtemp = forecast.list[i].main.temp_max;
        if (newmaxtemp > maxtemp) maxtemp = newmaxtemp;
        weathericon = forecast.list[i].weather[0].icon.substr(0,2);
        if (weatherlist.hasOwnProperty(weathericon)) {
            weatherlist[weathericon]++;
        } else {
            weatherlist[weathericon] = 1;
        }
    }
    if (dayArray.length > 5) dayArray.pop();
}

function clearAllText() {
    document.getElementById("day1").innerText = "--";
    document.getElementById("day2").innerText = "--";
    document.getElementById("day3").innerText = "--";
    document.getElementById("day4").innerText = "--";
    document.getElementById("day5").innerText = "--";
    document.getElementById("hi1").innerText = "--";
    document.getElementById("hi2").innerText = "--";
    document.getElementById("hi3").innerText = "--";
    document.getElementById("hi4").innerText = "--";
    document.getElementById("hi5").innerText = "--";
    document.getElementById("lo1").innerText = "--";
    document.getElementById("lo2").innerText = "--";
    document.getElementById("lo3").innerText = "--";
    document.getElementById("lo4").innerText = "--";
    document.getElementById("lo5").innerText = "--";
    document.getElementById("location").innerText = "";
    if (preferredunits != "k") {
        document.getElementById("temp").innerText = "--°";
    } else {
        document.getElementById("temp").innerText = "--";
    }
}

function clearAllIcons(iconid) {
    var iconuri = "Images/" + iconid + ".png";
    if (debug) {
        alert(iconuri);
    }
    document.getElementById("iconimg").src = iconuri;
    document.getElementById("weather1img").src = iconuri;
    document.getElementById("weather2img").src = iconuri;
    document.getElementById("weather3img").src = iconuri;
    document.getElementById("weather4img").src = iconuri;
    document.getElementById("weather5img").src = iconuri;
}

function getPrefs() {
    if (window.widget) {
        if (widget.preferenceForKey(widget.identifier + "-" + "currentLocation") === undefined) {
            widget.setPreferenceForKey(city, widget.identifier + "-" + "currentLocation");
            document.getElementById("city_field").value = city;
        }
        else {
            city = widget.preferenceForKey(widget.identifier + "-" + "currentLocation");
            document.getElementById("city_field").value = city;
        }
        if (widget.preferenceForKey(widget.identifier + "-" + "customLocationLabel") === undefined) {
            widget.setPreferenceForKey(customname, widget.identifier + "-" + "customLocationLabel");
            document.getElementById("custom_field").value = customname;
        }
        else {
            customname = widget.preferenceForKey(widget.identifier + "-" + "customLocationLabel");
            document.getElementById("custom_field").value = customname;
        }
        if (widget.preferenceForKey(widget.identifier + "-" + "widgetIsExpanded") === undefined) {
            widget.setPreferenceForKey(sizeToggle, widget.identifier + "-" + "widgetIsExpanded");
        }
        else {
            sizeToggle = widget.preferenceForKey(widget.identifier + "-" + "widgetIsExpanded");
            if (sizeToggle) {
                document.getElementById("middle-div").style.height = "124px";
                document.getElementById("bottom").style.top = "164px";
                document.getElementById("info").style.bottom = "67px";
                document.getElementById("shadow-front").style.height = "154px";
            } else {
                document.getElementById("middle-div").style.height = "20px";
                document.getElementById("bottom").style.top = "60px";
                document.getElementById("info").style.bottom = "171px";
                document.getElementById("shadow-front").style.height = "50px";
            }
        }
        if (widget.preferenceForKey(widget.identifier + "-" + "tempScale") === undefined) {
            widget.setPreferenceForKey(preferredunits, widget.identifier + "-" + "tempScale");
            if (preferredunits == "c") document.getElementById("popup").selectedIndex = 0;    
            if (preferredunits == "f") document.getElementById("popup").selectedIndex = 1;    
            if (preferredunits == "k") document.getElementById("popup").selectedIndex = 2;    
        }
        else {
            preferredunits = widget.preferenceForKey(widget.identifier + "-" + "tempScale");
            if (preferredunits == "c") document.getElementById("popup").selectedIndex = 0;    
            if (preferredunits == "f") document.getElementById("popup").selectedIndex = 1;    
            if (preferredunits == "k") document.getElementById("popup").selectedIndex = 2;    
        }
        if (widget.preferenceForKey("APIKey") === undefined) {
            apikey = "";
            widget.setPreferenceForKey(apikey, "APIKey");
            document.getElementById("api_field").value = apikey;
        }
        else {
            apikey = widget.preferenceForKey("APIKey");       
            document.getElementById("api_field").value = apikey;
        }
    }
}

function copyPrefs() {
    apikey = document.getElementById("api_field").value;
    city = document.getElementById("city_field").value;
    customname = document.getElementById("custom_field").value;
    preferredunits = document.getElementById("popup").value;
    setPrefs();
}

function copyPrefsToBackOfWidget() {
    document.getElementById("api_field").value = apikey;
    document.getElementById("city_field").value = city;
    document.getElementById("custom_field").value = customname;
    document.getElementById("popup").value = preferredunits;
}

function setPrefs() {
    if (window.widget) {
        widget.setPreferenceForKey(city, widget.identifier + "-" + "currentLocation");
        widget.setPreferenceForKey(customname, widget.identifier + "-" + "customLocationLabel");
        widget.setPreferenceForKey(sizeToggle, widget.identifier + "-" + "widgetIsExpanded");
        widget.setPreferenceForKey(preferredunits, widget.identifier + "-" + "tempScale");
        widget.setPreferenceForKey(apikey, "APIKey");
    }
}

function killPrefs() {
    if (window.widget) {
        widget.setPreferenceForKey(null, widget.identifier + "-" + "currentLocation");
        widget.setPreferenceForKey(null, widget.identifier + "-" + "customLocationLabel");
        widget.setPreferenceForKey(null, widget.identifier + "-" + "widgetIsExpanded");
        widget.setPreferenceForKey(null, widget.identifier + "-" + "tempScale");
    }
}

function killAllTimers() {
    if (debug) {
        alert("Stopping all timers");
    }
    if (weathertimer) {
        clearTimeout(weathertimer);
        weathertimer = null;
    }
    if (forecasttimer) {
        clearTimeout(forecasttimer);
        forecasttimer = null;
    }
}

function onPopUpChange() {
    preferredunits = document.getElementById("popup").value;
}

function overhaulBack() {
    document.getElementById("copyright_label1").innerHTML = "Icons: <a onClick='openSite(0);' title='dovora.com'>Dovora Interactive</a>, <a onClick='openSite(1);' title='Creative Commons Attribution License 4.0 International'>CC-BY 4.0 Int\'l";
}

function openSite(s)
{
    widget.openURL(sites[s]);
    if (debug)
    {
        alert ("openSite()");
    }
}

function openThirdSite(event) {
    openSite(2);
}
