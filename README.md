# OpenWeather #
As you might have noticed, the Weather widget in macOS's Dashboard has stopped working. Yahoo! retired their original API, and Apple is not going to update the widget, as Dashboard will be removed from macOS 10.15 Catalina. So for those of us who still like some weather info at their fingertips in the meantime, or for us old-schoolers who still like to do things the old-fashioned way, here's a replacement for it that uses OpenWeatherMap.org, under an MIT License. Ready to use binary is available in the downloads section. Enjoy!

You will need to bring your own API key, but that's easy: just register for one at OpenWeatherMap.org - the free plan will do nicely. Then, drop one widget on your Dashboard, and enter your new API key on the back of it.

Your API key is shared among all instances of the widget, so please complete the above step, then flip over the widget so that the front is visible. You can now add more instances if you so desire. Your API key will be remembered even if you remove all widgets, and automatically used when you create a fresh widget.

Some points to keep in mind:

* For the most part, the widget will work like you would expect it to, based on your earlier experience with the original Weather widget - you can shrink it to just view the current weather, or expand it to get a five day forecast. Yes, that's one day less than you'd get from the Apple widget. If it were still working.
* Service is update once every 10 minutes, so the internal timer for the widget reflects that;
* Data requests are delayed one second in order to meet with OpenWeatherMap's requirement to keep the number of requests within 60 a minute. You could ostensibly quickly open and close Dashboard quickly enough to constantly request data from OpenWeatherMap, which can get your account blocked. The delay is meant to prevent this. Two calls to the API are made every 10 minutes if the widget is visible, spaced a second apart, with one second of delay leading in.
* Therefore, in order to not get blocked, don't make more than about 30 instances ;).

Tested under macOS 10.6.8 Snow Leopard and 10.13.6 High Sierra, but should work on everything since macOS 10.4.3.

Weather icons are from Dovora Interactive (dovora.com) under a Creative Commons Attribution 4.0 International license. No changes were made to the artwork, but the filenames were adapted.


Building
--------
Editing and deploying can be done from either version of DashCode, or you can
edit the files within the widget bundle with your favourite text/image editors. 

Version info
------------
Current version is 1.0.
